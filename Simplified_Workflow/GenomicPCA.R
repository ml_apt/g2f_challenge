## ---- SETUP : -----------------------------------------------------------------

# setwd(dir = "./Simplified_Workflow/")

# library(data.table)
# library(tidyverse)
# library(FactoMineR) # pour effectuer l'ACP
# library(factoextra)

if (!".HardWriter" %in% ls(all.names = T)) {
  .HardWriter <- F
}

## ------------------------------------------------------------------------------
# pour extraire et visualiser les résultats issus de FactoMineR


## -----------------------------------------------------------------------------------------------------
## Load the data
if ("Training_Geno_Small" %in% ls()) {
  Geno <- Training_Geno_Small
} else {
  Geno <- Training_Geno_Small <- fread("../Filtered_Training_Data/Geno_SMALL.txt")
}

## Scale the data
Means <- rowMeans(Geno)
StdErr <-
  {
    (Geno - Means)**2
  } %>%
  rowMeans() %>%
  sqrt()
Geno.scale <-
  {
    (Geno - Means) / StdErr
  } %>% t()

## Check
# colMeans(Geno.scale[, 1:10])
# apply(Geno.scale[, 1:10], 2, var)


## -----------------------------------------------------------------------------------------------------
res.pca <- PCA(Geno.scale, scale.unit = FALSE, graph = FALSE, ncp = 100)
# names(res.pca)


## -----------------------------------------------------------------------------------------------------
## Plot the
# fviz_eig(res.pca, addlabels = TRUE, ylim = c(0, 15), ncp = 15)


## -----------------------------------------------------------------------------------------------------
## Coordinates on the PCA axes
Coord.train <- get_pca_ind(res.pca)$coord %>%
  as.data.frame() %>%
  rownames_to_column("Hybrid")
# plot(Coord.train$Dim.1, Coord.train$Dim.2, cex = 0.4, xlab = "Axis1", ylab = "Axis2")


## -----------------------------------------------------------------------------------------------------
## Load the test dataset
if ("Testing_Geno_Small" %in% ls()) {
  Geno.test <- Testing_Geno_Small
} else {
  Geno.test <- Testing_Geno_Small <- fread("../Filtered_Testing_Data/Geno_SMALL.txt")
}

# dim(Geno.test)
# sum(is.na(Geno.test))
## Scale the data
Geno.test.scale <-
  {
    (Geno.test - Means) / StdErr
  } %>% t()

## Get the weights
Weights <- res.pca$svd$V
Coord.test <- Geno.test.scale %*% Weights %>%
  as.data.frame() %>%
  rownames_to_column("hybrid")
names(Coord.test) <- names(Coord.train)

## Data representation
# ggplot(data = Coord.train, aes(x = Dim.1, y = Dim.2)) +
#   geom_point() +
#   geom_point(data = Coord.test, col = 2)
# ggplot(data = Coord.train, aes(x = Dim.2, y = Dim.3)) +
#   geom_point() +
#   geom_point(data = Coord.test, col = 2)


## -----------------------------------------------------------------------------------------------------
res_file <- "../Filtered_Training_Data/PCA_coordinates.txt"
Training_Genomic_PCA <- Coord.train

if (!file.exists(res_file) || .HardWriter) {
  fwrite(x = Coord.train, file = res_file)
}


res_file <- "../Filtered_Testing_Data/PCA_coordinates.txt"
Testing_Genomic_PCA <- Coord.test

if (!file.exists(res_file) || .HardWriter) {
  fwrite(x = Testing_Genomic_PCA, file = res_file)
}

toErase <- character()
toErase <- ls()[!ls() %in% c(
  "Testing_Trait_Data", "Training_Trait_Data",
  "Testing_Genomic_PCA", "Training_Genomic_PCA"
)]
rm(list = toErase)
gc()

# ## If needed, reload the data
# Coord.train <- fread(file = "../Filtered_Training_Data/PCA_coordinates.txt")
# Coord.test <- fread(file = "../Filtered_Testing_Data/PCA_coordinates.txt")
#
#
# ## -----------------------------------------------------------------------------------------------------
# ## Make a quick and durty data representation of the families
# res.kmeans <- Coord.train %>%
#   select(-Hybrid) %>%
#   kmeans(., centers = 20, nstart = 50, iter.max = 100)
# Coord.train %>%
#   mutate(Clusters = as.factor(res.kmeans$cluster)) %>%
#   ggplot(aes(x = Dim.1, y = Dim.2, col = Clusters)) +
#   geom_point()
#
# ## Get the names of the hybrids belonging to the right cluster
# HybName.C1 <- Coord.train %>%
#   filter(Dim.1 > 70) %>%
#   pull(Hybrid)
# P2.C1 <- str_split(HybName.C1, pattern = "/") %>% map_chr(~ .x[2])
# table(P2.C1)
#
# ## Get the names of the hybrids belonging to the upper cluster
# HybName.C2 <- Coord.train %>%
#   filter(Dim.2 > 50) %>%
#   pull(Hybrid)
# P2.C2 <- str_split(HybName.C2, pattern = "/") %>% map_chr(~ .x[2])
# table(P2.C2)
#
# ## How does the graph look like without these 2 sets of hybrids ?
# Hyb.PHT69.1 <- HybName.C1 %>%
#   str_subset(pattern = "PHT69")
# Hyb.PHZ51.1 <- HybName.C2 %>%
#   str_subset(pattern = "PHZ51")
# Coord.train %>%
#   filter(!Hybrid %in% c(Hyb.PHT69.1, Hyb.PHZ51.1)) %>%
#   ggplot(aes(x = Dim.1, y = Dim.2)) +
#   geom_point() +
#   geom_point(data = Coord.test, col = 2)
#
# ## Names of the hybrids to remove
# Hybrids.rmv <- Coord.train %>%
#   filter(Hybrid %in% c(Hyb.PHT69.1, Hyb.PHZ51.1)) %>%
#   pull(Hybrid)
#
#
# ## -----------------------------------------------------------------------------------------------------
# names(Geno.test) %>% str_subset(pattern = "PHT69")
# names(Geno.test) %>% str_subset(pattern = "PHZ51")
#
#
# ## -----------------------------------------------------------------------------------------------------
# ## Load the training phenotypes
# Pheno.train <- fread("../Filtered_Training_Data/Trait_Data.txt")
#
#
# ## -----------------------------------------------------------------------------------------------------
# Env.rmv <- Pheno.train %>%
#   filter(Hybrid %in% Hybrids.rmv) %>%
#   pull(Env) %>%
#   unique()
# Env.rmv %>% length()
#
#
# ## -----------------------------------------------------------------------------------------------------
# GetMostUsedParent <- function(Hyb) {
#   P1 <- str_split(Hyb, pattern = "/") %>% map_chr(~ .x[1])
#   P2 <- str_split(Hyb, pattern = "/") %>% map_chr(~ .x[2])
#   Res <- c(P1, P2) %>%
#     unique() %>%
#     setNames(., .) %>%
#     map_dbl(~ str_detect(Hyb, .x) %>% sum()) %>%
#     {
#       .[which.max(.)]
#     } %>%
#     names()
#   return(Res)
# }
# PropOfMostusedParent <- Pheno.train %>%
#   group_by(Env) %>%
#   summarize(
#     MostUsedParent = GetMostUsedParent(Hybrid),
#     NbHyb = str_detect(Hybrid, MostUsedParent) %>% sum(),
#     NbMeas = n(),
#     Prop = NbHyb / NbMeas
#   )
#
# PropOfMostusedParent %>%
#   filter(Prop >= 0.9) %>%
#   print(n = Inf)
# TopParents <- PropOfMostusedParent %>%
#   pull(MostUsedParent) %>%
#   unique()
# TopParents
#
#
# ## -----------------------------------------------------------------------------------------------------
# ## Build a new training set
# Train <- Pheno.train
#
# ## Build indicator functions for each major parent
# for (parent in TopParents) {
#   Train[[paste0("Parent_", parent)]] <- str_detect(Train$Hybrid, parent) %>% as.numeric()
# }
#
# ## Count the number of times they were crossed
# Train %>%
#   select(contains("Parent_")) %>%
#   colSums()
#
# ## And checked how much they were crossed together
# Train %>%
#   select(contains("Parent_")) %>%
#   rowSums() %>%
#   table()
#
#
# ## -----------------------------------------------------------------------------------------------------
# Test <- fread("../Testing_Data/1_Submission_Template_2022.csv")
# ## Build indicator functions for each major parent
# for (parent in TopParents) {
#   Test[[paste0("Parent_", parent)]] <- str_detect(Test$Hybrid, parent) %>% as.numeric()
# }
#
# ## Count the number of times they were crossed
# Test %>%
#   select(contains("Parent_")) %>%
#   colSums()
#
# ## And checked how much they were crossed together
# Test %>%
#   select(contains("Parent_")) %>%
#   rowSums() %>%
#   table()
#
#
#
# ## -----------------------------------------------------------------------------------------------------
# P1 <- Test$Hybrid %>%
#   str_split(pattern = "/") %>%
#   map_chr(~ .x[1])
# P2 <- Test$Hybrid %>%
#   str_split(pattern = "/") %>%
#   map_chr(~ .x[2])
# Parent.test <- c(P1, P2)
# table(Parent.test) %>%
#   {
#     .[order(., decreasing = TRUE)]
#   } %>%
#   head()
#
# P1.train <- Train$Hybrid %>%
#   str_split(pattern = "/") %>%
#   map_chr(~ .x[1])
# P2.train <- Train$Hybrid %>%
#   str_split(pattern = "/") %>%
#   map_chr(~ .x[2])
# Parent.train <- c(P1.train, P2.train)
# str_detect(Parent.train, pattern = "LH244") %>% sum()
#
#
# ## -----------------------------------------------------------------------------------------------------
# Parent.ind.train <- Train %>%
#   select(contains("Parent_"))
# HowManyParents.train <- Parent.ind.train %>%
#   rowSums()
# WhichParent <- TopParents[apply(Parent.ind.train, 1, which.max)]
# Train <- Train %>%
#   mutate(WhichParent = WhichParent) %>%
#   mutate(Parent = case_when(
#     (HowManyParents.train == 0) ~ "None",
#     (HowManyParents.train == 1) ~ WhichParent,
#     (HowManyParents.train == 2) ~ "Several"
#   ))
# Coord.train %>%
#   left_join(., Train %>% select(Hybrid, Parent) %>% distinct(), by = "Hybrid") %>%
#   mutate(ShortParent = str_sub(Parent, 1, 2)) %>%
#   ggplot(aes(x = Dim.1, y = Dim.2, col = ShortParent)) +
#   geom_point()
#
#
# ## -----------------------------------------------------------------------------------------------------
# Pheno.train <- Pheno.train %>%
#   mutate(
#     Tester = Hybrid %>%
#       str_split(., pattern = "/") %>%
#       map_chr(~ .x[2]),
#     Complement = Hybrid %>%
#       str_split(., pattern = "/") %>%
#       map_chr(~ .x[1]),
#     Tester.Family = str_extract(Tester, "[A-Z]+")
#   )
#
# ## How many testers ? Tester families ? Complements ?
# length(unique(Pheno.train$Tester))
# length(unique(Pheno.train$Tester.Family))
# length(unique(Pheno.train$Complement))
#
# Pheno.train %>%
#   pull(Tester) %>%
#   table() %>%
#   {
#     .[order(., decreasing = TRUE)]
#   }
# Pheno.train %>%
#   pull(Tester.Family) %>%
#   table()
# Pheno.train %>%
#   pull(Complement) %>%
#   table() %>%
#   {
#     .[order(., decreasing = TRUE)]
#   } %>%
#   .[1:50]
#
#
# ## -----------------------------------------------------------------------------------------------------
# Total.train <- Pheno.train %>%
#   left_join(., Coord.train, by = "Hybrid")
# fwrite(Total.train, "../Filtered_Training_Data/TraitsAndPCA.txt")
#
# ggplot(Total.train, aes(x = Dim.1, y = Dim.2, col = Tester.Family)) +
#   geom_point()
#
#
# ## -----------------------------------------------------------------------------------------------------
# Test <- Test %>%
#   mutate(
#     Tester = Hybrid %>%
#       str_split(., pattern = "/") %>%
#       map_chr(~ .x[2]),
#     Complement = Hybrid %>%
#       str_split(., pattern = "/") %>%
#       map_chr(~ .x[1]),
#     Tester.Family = str_extract(Tester, "[A-Z]+")
#   )
#
# Test <- left_join(Test, Coord.test, by = "Hybrid")
# fwrite(Test, "../Filtered_Testing_Data/TraitsAndPCA.txt")
#
#
# ## How many testers ? Tester families ? Complements ?
# length(unique(Test$Tester))
# length(unique(Test$Tester.Family))
# length(unique(Test$Complement))
#
# Test %>%
#   pull(Tester) %>%
#   table() %>%
#   {
#     .[order(., decreasing = TRUE)]
#   }
# Test %>%
#   pull(Tester.Family) %>%
#   table()
# setdiff(unique(Test$Tester.Family), unique(Pheno.train$Tester.Family))
# setdiff(unique(Test$Tester), unique(Pheno.train$Tester))
