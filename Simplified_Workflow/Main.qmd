---
title: "Programme principal"
format: 
  html: 
    toc: true
    number-sections: true
    code-fold: true
    code-tools: true
execute: 
  eval: true
  freeze: auto
  cache: true
---

# Main Setup

### Packages

```{r echo=FALSE, include=FALSE}
check_package <- function(package,version){
  is_installed <- 'dplyr' %in% rownames(installed.packages())
  is_upper <- is_installed &&
    utils::compareVersion(installed.packages()['dplyr','Version'],version) >= 0
  if(!is_installed){
    stop("Please install ",package," package first")
  }
  if(!is_upper){
    stop(package,' version is lower than ',version,"\nThis version is mandatory, please install it")
  }
  return(T)
}
```

```{r check-dplyr-version, include=FALSE}
check_package('dyplr','1.1.0')
```


```{r packages}
#| message: false
# Librairies utilisées
library(SpATS)
library(emmeans)
library(lme4)
library(data.table)
library(tidyverse)
library(future)
library(purrr)
library(furrr)
library(tictoc)
library(lubridate)
library(rsample)
library(randomForest)
library(randomForestExplainer)
library(FactoMineR)
library(factoextra)
library(glmnet)

# library(ggforce)
# library(ggrepel)
# library(corrplot)
# library(learnMET)
```

### Directory Setup & Function definition

```{r SETUP}
# Working Directory For all scripts :
# setwd(dir = "./Simplified_Workflow/")

# Folder for produced intermediary Files :
if (!dir.exists("../Filtered_Training_Data")) {
  dir.create(path = "../Filtered_Training_Data")
}
if (!dir.exists("../Filtered_Testing_Data")) {
  dir.create(path = "../Filtered_Testing_Data")
}
if (!dir.exists("../Results")) {
  dir.create(path = "../Results")
}

# To check on running time
.tic <- Sys.time()
.tac <- function(tic) {
  toc <- Sys.time()
  print('Running Time :')
  print(toc - tic)
  return(toc)
}
```

### Parameters

Boolean parameter to force the writing of intermediary files when they are already written.

```{r PARAMETERS}
.HardWriter <- F
```


# Allelic Frequences

Reduce the dimension of genomic data. Build the "Training_Geno_Small" & "Testing_Geno_Small" data frames.

```{r frequences-alleliques}
#| file: 'frequences_alleliques.R'
#| cache-rebuild: false
#| cache-comments: true
#| message: false
#| autodep: true
```


```{r}
.tic <- .tac(.tic)
```



# Genomic PCA

Do PCA with "Training_Geno_Small" & "Testing_Geno_Small" data frames, store the 100 first dimensions in "Testing_Genomic_PCA" & "Training_Genomic_PCA".

```{r pca}
#| file: 'GenomicPCA.R'
#| cache-rebuild: false
#| cache-comments: true
#| message: false
#| autodep: true
```


```{r}
.tic <- .tac(.tic)
```


# AnalysePheno

Cleaning of Training_Trait_Data by erasing problematic environments (Location + Year), splitting environment with great gaps between minimal and maximal sawing dates. Correcting the spacial bias. Store this in "Trait_Data". 

```{r AnalysePheno}
#| file: 'AnalysePheno.R'
#| cache-rebuild: false
#| cache-comments: true
#| message: false
#| autodep: true
```


```{r}
.tic <- .tac(.tic)
```



# Meta Data Treatment

Cleaning of Testing_Meta_Data & Training_Meta_Data. Correcting writing mistakes, merging some categories, erasing NA's, etc...

```{r Meta-Data-Treatment}
#| file: 'Meta_Data_Treatment_Theo.R'
#| cache-rebuild: false
#| cache-comments: true
#| message: false
#| autodep: true
```


```{r}
.tic <- .tac(.tic)
```


# Cleaning Trait Data

Intermediary files are produces giving the right environment names and other information that would be needed to correct all tables environments. Disease Trials, Environment to split and Corrected names

```{r script-get-pheno}
#| file: 'script_get_pheno.R'
#| cache-rebuild: false
#| cache-comments: true
#| message: false
#| autodep: true
```


```{r}
.tic <- .tac(.tic)
```


# Main merging soil & meta data

This script is correcting environment names. It then merge cleaned meta data and cleaned soil data. Creating : "merged_env_soil_train" &  merged_env_soil_test"

```{r main-merging-meta-soil}
#| file: 'main_merging_meta_soil.R'
#| cache-rebuild: false
#| cache-comments: true
#| message: false
#| autodep: true
```


```{r}
.tic <- .tac(.tic)
```

# Weather data cleaning

Cleaning Weather data, start first by correcting environments names then fill complete the full year when some days are missing. Make a PCA of environments with weather data. 

```{r weather-data-transformation}
#| file: 'Weather_data_transformation.R'
#| cache-rebuild: false
#| cache-comments: true
#| message: false
#| autodep: true
```


```{r}
.tic <- .tac(.tic)
```


# Flowering Date Analysis

Cleaning Silking Dates of training data and predicting for test data. We used a linear model based on accumulated Thermal Time.

```{r FlowringDateAnalysis}
#| file: 'FlowringDateAnalysis.R'
#| cache-rebuild: false
#| cache-comments: true
#| message: false
#| autodep: true
```

```{r}
.tic <- .tac(.tic)
```

  
# Predict Test Sowing Dates

Predicting Sowing dates for test data using Environment and Localisation. With a simple linear model.

```{r PredictPlantingDates}
#| file: 'PredictPlantingDates.R'
#| cache-rebuild: false
#| cache-comments: true
#| message: false
#| autodep: true
```


```{r}
.tic <- .tac(.tic)
```


# Weather by environnement analysis

Gathering Meta x Soil data with Weather Data. The weather Data is coded in two different way : 
 - For 3 different "Biological" states (Before Silking / 1 month Around Silking / 2 months after Silking) we gathered variables 21 summarizing weather. 
 - Taking the first dimension of weather PCA. 
Store this results in "Env_Train" and "Env_Test" tables.

```{r Env_data_gathering}
#| file: 'Weather_by_environnement_analysis.R'
#| cache-rebuild: false
#| cache-comments: true
#| message: false
#| autodep: true
```

```{r}
.tic <- .tac(.tic)
```


# Iterative Random Forest

Final model : 

  - Linear model by localisation
  - Using Residuals of this model to feed 3 Random Forests :
    1. Apply the Random Forest on Genomic Data (Training_Genomic_PCA)
    2. Apply the Random Forest on Environmental Data (Env_Train)
    3. Apply RF on both Genomic and Environmental Data (Env_Train + Training_Genomic_PCA)
    
Compare those 4 models (LM + 3 RF) with cross validation using each year as test-set.
Prediction with those four models on real test set and storing results.

```{r IterativeRF}
#| file: 'IterativeRF.R'
#| cache-rebuild: false
#| cache-comments: true
#| message: false
#| autodep: true
```

```{r}
.tic <- .tac(.tic)
```

Based on this work and the produced table "Results/MSE_iterative_random_forest.csv" we identified the Iterative Random Forest on genomic Data as the best on our Cross Validation Experiment. This is the model we choose to present, the results are stored in "Results/Prediction_lg.csv".

[Results are stored this way :]{style="text-decoration: underline;"}

  1. Simple Linear Model on Localisation = "Results/Prediction_l.csv"
  2. [RF (Localisation + Genomic Data) = "Results/Prediction_lg.csv"]{style="color: red"}
  3. RF (Localisation + Environmental Data) = "Results/Prediction_le.csv"
  4. RF (Localisation + Genomic Data + Environmental Data) = "Results/Prediction_leg.csv"

