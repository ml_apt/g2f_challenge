## ---- SETUP : -----------------------------------------------------------------

# setwd(dir = "./Simplified_Workflow/")
# library(data.table)
# library(tidyverse)
# library(lubridate)

## ------------------------------------------------------------------------------


## ----load data--------------------------------------------------------------------------

if ("Training_Trait_Data" %in% ls()) {
  pheno <- Training_Trait_Data
} else {
  pheno <- Training_Trait_Data <- fread("../Training_Data/1_Training_Trait_Data_2014_2021.csv", sep = ",", header = T)
}

pheno <- pheno %>%
  select(1:10, Date_Planted, Yield_Mg_ha, Pollen_DAP_days, Silk_DAP_days, Date_Harvested)

## ----get_disease_trial-----------------------------------------------------------------------
if ("Old_Training_Meta_Data" %in% ls()) {
  disease_trial_env <- Old_Training_Meta_Data
} else {
  disease_trial_env <- Old_Training_Meta_Data <- fread("../Training_Data/2_Training_Meta_Data_2014_2021.csv", sep = ",", header = T)
}

disease_trial_env <- disease_trial_env %>%
  filter(Treatment == "Disease trial") %>%
  pull(Env)

# disease_trial_env
pheno <- filter(pheno, !Env %in% disease_trial_env)


## ----choose trait-----------------------------------------------------------------------
## Change it for the present analysis (it will be renamed back at the end of the script)
pheno <- pheno %>%
  rename(trait = Yield_Mg_ha) %>%
  filter(!is.na(trait))


## ----sowing 1---------------------------------------------------------------------------
pheno <- pheno %>%
  mutate(SowingDate = parse_date_time(pheno$Date_Planted, orders = c("%m/%d/%y", "%y-%m-%d")) %>%
    as_date()) %>%
  mutate(HarvestDate = parse_date_time(pheno$Date_Harvested, orders = c("%m/%d/%y", "%y-%m-%d")) %>%
    as_date())

## Simple check
# pheno %>%
#   mutate(CheckYear= year(SowingDate) != Year) %>%
#   filter(CheckYear)

EnvToSplit <- pheno %>%
  group_by(Env) %>%
  summarize(
    NbRep = length(unique(Replicate)),
    NbSowingDate = length(unique(SowingDate)),
    MinSowingDate = min(SowingDate),
    MaxSowingDate = max(SowingDate),
    SowingGap = MaxSowingDate - MinSowingDate
  ) %>%
  arrange(desc(SowingGap)) %>%
  # print() %>%
  filter(SowingGap > 7) %>%
  select(Env, MinSowingDate, MaxSowingDate)




## ----sowing 2---------------------------------------------------------------------------
## Nb Env before
# length(unique(pheno$Env))

for (ii in 1:nrow(EnvToSplit)) {
  Values <- EnvToSplit[ii, ] %>% as.list()
  pheno <- pheno %>%
    mutate(Env = replace(Env, Env == Values$Env & SowingDate == Values$MinSowingDate, paste0(Values$Env, "_Early"))) %>%
    mutate(Env = replace(Env, Env == Values$Env & SowingDate == Values$MaxSowingDate, paste0(Values$Env, "_Late")))
}

## Nb Env after
# length(unique(pheno$Env))

NbRepSplit <- pull(EnvToSplit, Env) %>%
  outer(., c("_Late", "_Early"), paste0) %>%
  as.vector() %>%
  {
    filter(.data = pheno, Env %in% .)
  } %>%
  group_by(Env) %>%
  summarize(NbHyb = length(unique(Hybrid)), NbMeas = n()) %>%
  mutate(NbRep = NbMeas / NbHyb)
# NbRepSplit


## ---------------------------------------------------------------------------------------
pheno <- NbRepSplit %>%
  filter(NbRep < 1.2) %>%
  pull(Env) %>%
  {
    filter(pheno, !Env %in% .)
  }

toErase <- character()
toErase <- ls()[!ls() %in% c(
  "Training_Soil_Data",
  "Trait_Data", "train_meta_table",
  "Old_Testing_Meta_Data", "Old_Training_Meta_Data",
  "Testing_Trait_Data", "Training_Trait_Data",
  "Testing_Meta_Data", "Training_Meta_Data",
  "Testing_Genomic_PCA", "Training_Genomic_PCA",
  "pheno", "EnvToSplit", "disease_trial_env"
)]
rm(list = toErase)
