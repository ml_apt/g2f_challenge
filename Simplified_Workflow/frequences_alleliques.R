## ---- SETUP : -----------------------------------------------------------------

# setwd(dir = "./Simplified_Workflow/")

# library(tidyverse) # Pour la manipulation des données
# library(data.table) # Pour la gestion de données en grande dimension

if (!".HardWriter" %in% ls(all.names = T)) {
  .HardWriter <- F
}

## ------------------------------------------------------------------------------


if ("Training_Trait_Data" %in% ls()) {
  train_hybrids_name <- Training_Trait_Data
} else {
  train_hybrids_name <- Training_Trait_Data <- fread("../Training_Data/1_Training_Trait_Data_2014_2021.csv")
}

## ----train_hybrids_name-----------------------------------------------------------
train_hybrids_name <- train_hybrids_name %>%
  filter(!(Hybrid %in% c("M0125/CG102", "PHW03/PHRE1"))) %>% # 2 hybrides vus nulle part
  pull(Hybrid) %>% # on extrait les hybrides
  unique() # uniques


## ----transform_to_012-------------------------------------------------------------
transform_to_012 <- function(x) {
  # Le cas "./." sera traité comme NA
  case_when(
    x == "0/0" ~ 0,
    x == "1/1" ~ 2,
    x %in% c("0/1", "1/0") ~ 1
  )
}


## ----initial_table----------------------------------------------------------------
initial_table <- fread("../Training_Data/5_Genotype_Data_All_Years.vcf",
  skip = 22
) %>%
  # On ne selectionne que les colonnes dans train_hybrids_name
  select_at(vars(1:9, intersect(colnames(.), train_hybrids_name))) %>%
  filter(nchar(ALT) == 1) %>%
  # On vire les SNPS non bialléliques (2.2% des lignes)
  # On transforme toutes les colonnes de SNP
  mutate_at(10:ncol(.), transform_to_012)
gc() # On fait un garbage collector (c'est lourd en memoire)


## ---------------------------------------------------------------------------------
NbNAperSnp <- initial_table %>%
  select(-c(1:9)) %>%
  is.na() %>%
  rowSums()


## ----quantiles_NA-----------------------------------------------------------------
# quantile(NbNAperSnp,
#   probs = c(.01, .05, .1, .15, .2, .25, .3, 0.5, .75, .9, .95, 1)
# )


## ----table_NA---------------------------------------------------------------------
# table(NbNAperSnp > 0.05 * 4421)


## ---------------------------------------------------------------------------------
initial_table <- initial_table %>%
  filter(NbNAperSnp <= 0.05 * 4421)
# dim(initial_table)


## ----frequences_alleliques--------------------------------------------------------
frequences_alleliques <- select(initial_table, -(1:9)) %>%
  rowMeans(na.rm = TRUE) %>%
  {
    . * 0.5
  } # Pour ramener entre 0 et 1
gc() # même topo


## ----histograme_frequences_alleliques---------------------------------------------
# hist(frequences_alleliques,
#   prob = TRUE,
#   breaks = seq(0, 1, by = .005),
#   main = "Distribution des fréquences alléliques",
#   xlab = "Fréquence", ylab = "Densité empirique"
# )


## ----quantiles_frequences_alleliques----------------------------------------------
# quantile(frequences_alleliques,
#   probs = c(.01, .05, .1, .15, .2, .25, .3, 0.5, .75, .95, 1)
# )


## ---------------------------------------------------------------------------------
# table(frequences_alleliques == 0 | frequences_alleliques == 1)
# table(frequences_alleliques < 0.02 | frequences_alleliques > 0.98)


## ---------------------------------------------------------------------------------
SnpRemoved.idx <- frequences_alleliques >= 0.02 & frequences_alleliques <= 0.98
initial_table <- initial_table %>%
  filter(SnpRemoved.idx)
# dim(initial_table)

## On conserve les frequences alléliques des marqueurs gardes
frequences_alleliques <- frequences_alleliques[SnpRemoved.idx]
# length(frequences_alleliques)
# summary(frequences_alleliques, 100)


## ---------------------------------------------------------------------------------
NbNAperSnp <- initial_table %>%
  select(-c(1:9)) %>%
  is.na() %>%
  rowSums()
# table(NbNAperSnp >= 10)


## ---------------------------------------------------------------------------------
input_NA <- function(x) {
  idx <- is.na(x) %>% which()
  x[idx] <- 2 * frequences_alleliques[idx]
  return(x)
}

initial_table <- initial_table %>%
  mutate_at(10:ncol(.), input_NA)
gc() # On fait un garbage collector (c'est lourd en memoire)

## Verification
# initial_table %>%
#   select(-c(1:9)) %>%
#   is.na() %>%
#   sum()


## ---------------------------------------------------------------------------------


# Creation du fichier Info.csv
res_file <- "../Filtered_Training_Data/Info.txt"
Info <- initial_table %>%
  select_at(vars(1:9)) %>%
  rename(CHROM = "#CHROM")

if (!file.exists(res_file) | .HardWriter) {
  fwrite(x = Info, file = res_file)
}

## Creation du fichier Geno.csv
res_file <- "../Filtered_Training_Data/Geno.txt"
Geno <- initial_table %>%
  select(contains("/"))

if (!file.exists(res_file) | .HardWriter) {
  fwrite(x = Geno, file = res_file)
}

## Creation d'un fichier ou l'on garde les infos de MAF
res_file <- "../Filtered_Training_Data/Freq.txt"
Freq <- initial_table %>%
  select_at(vars(1:3)) %>%
  rename(CHROM = "#CHROM") %>%
  mutate(Freq = frequences_alleliques)

if (!file.exists(res_file) | .HardWriter) {
  fwrite(x = Freq, file = res_file)
}





## ---------------------------------------------------------------------------------
SNPsel.idx <- seq(1, nrow(Info), 10)


res_file <- "../Filtered_Training_Data/Info_SMALL.txt"
Info_SMALL <- Info[SNPsel.idx, ]

if (!file.exists(res_file) | .HardWriter) {
  fwrite(x = Info_SMALL, file = res_file)
}

res_file <- "../Filtered_Training_Data/Geno_SMALL.txt"
Training_Geno_Small <- Geno %>%
  .[SNPsel.idx, ]

if (!file.exists(res_file) | .HardWriter) {
  fwrite(x = Training_Geno_Small, file = res_file)
}


res_file <- "../Filtered_Training_Data/Freq_SMALL.txt"
Freq_SMALL <- Freq[SNPsel.idx, ]

if (!file.exists(res_file) | .HardWriter) {
  fwrite(x = Freq_SMALL, file = res_file)
}


## ---------------------------------------------------------------------------------
toErase <- character()
toErase <- ls()[!ls() %in% c(
  "Info_SMALL", "Freq",
  "Training_Trait_Data", "transform_to_012",
  "input_NA", "Training_Geno_Small"
)]
rm(list = toErase)
gc()


## ---------------------------------------------------------------------------------
if ("Testing_Trait_Data" %in% ls()) {
  test_hybrids_name <- Testing_Trait_Data
} else {
  test_hybrids_name <- Testing_Trait_Data <- fread("../Testing_Data/1_Submission_Template_2022.csv")
}


test_hybrids_name <- test_hybrids_name %>%
  pull(Hybrid) %>% # on extrait les hybrides
  unique() # uniques
# length(test_hybrids_name)


## ---------------------------------------------------------------------------------
## Marqueurs sélectionnés sur la base du training set
SNP.list <- Freq %>% pull(ID)
frequences_alleliques <- Freq %>% pull(Freq)

test_set <- data.table::fread("../Training_Data/5_Genotype_Data_All_Years.vcf",
  skip = 22
) %>%
  # On ne selectionne les hybrides test
  select_at(vars(1:9, intersect(colnames(.), test_hybrids_name))) %>%
  # On filtre les marqueurs
  filter(ID %in% SNP.list) %>%
  # On transforme en 0,1,2
  mutate_at(10:ncol(.), transform_to_012)
gc()
# dim(test_set)



## ---------------------------------------------------------------------------------
## Proportion of NA's ?
NbNA <- test_set %>%
  select(contains("/")) %>%
  is.na() %>%
  sum()
# NbNA / (nrow(test_set) * ncol(test_set))


## ---------------------------------------------------------------------------------
test_set <- test_set %>%
  mutate_at(10:ncol(.), input_NA)


## Verification
# test_set %>%
#   select(-c(1:9)) %>%
#   is.na() %>%
#   sum()


## ---------------------------------------------------------------------------------

## Creation du fichier Geno.csv
res_file <- "../Filtered_Testing_Data/Geno.txt"
Geno_Test <- test_set %>%
  select(contains("/"))

if (!file.exists(res_file) | .HardWriter) {
  fwrite(x = Geno_Test, file = res_file)
}



# Avec un sous echantillon aussi

SNP_SMALL <- Info_SMALL %>%
  pull(ID)


res_file <- "../Filtered_Testing_Data/Geno_SMALL.txt"
Testing_Geno_Small <- test_set %>%
  filter(ID %in% SNP_SMALL) %>%
  select(contains("/"))

if (!file.exists(res_file) | .HardWriter) {
  fwrite(x = Testing_Geno_Small, file = res_file)
}

toErase <- character()
toErase <- ls()[!ls() %in% c(
  "Testing_Trait_Data",
  "Training_Trait_Data", "Training_Geno_Small",
  "Testing_Geno_Small"
)]
rm(list = toErase)
gc()
